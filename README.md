# open-cuts-reporter

[![ci](https://gitlab.com/open-cuts/open-cuts-reporter/badges/master/pipeline.svg)](https://gitlab.com/open-cuts/open-cuts-reporter/) [![coverage report](https://gitlab.com/open-cuts/open-cuts-reporter/badges/master/coverage.svg)](https://ubports.gitlab.io/installer/open-cuts-reporter/coverage/) [![npm](https://img.shields.io/npm/v/open-cuts-reporter)](https://www.npmjs.com/package/open-cuts-reporter)

An npm package that provides convenient smart reporting to [OPEN-CUTS](https://www.open-cuts.org), the open crowdsourced user testing suite.

## Usage

Install the library by running `npm i open-cuts-reporter`.

```javascript
const { OpenCutsReporter } = require("open-cuts-reporter");
const openCutsReporter = new OpenCutsReporter({
  url: "https://ubports.open-cuts.org", // base url
  token: "1337" // optional access token
});
openCutsReporter
  .smartRun(
    "5e9d746c6346e112514cfec7", // hard-coded system id
    "5e9d75406346e112514cfeca", // hard-coded test id
    "0.7.4-beta", // build tag, the corresponding id will be inferred
    {
      result: "PASS",
      comment: "the horse does not eat cucumber salad",
      combination: [
        {
          variable: "Environment",
          value: "Linux"
        },
        {
          variable: "Package",
          value: "AppImage"
        }
      ],
      logs: [
        {
          name: "ubports-installer.log",
          content: "the quick brown fox jumped over the lazy dog"
        }
      ]
    }
  )
  .then(url => console.log(`run created at ${url}`))
  .catch(error => console.error(`error: ${error}`));
```

### Documentation

You can build documentation by running `npm run docs`. Then, open `./docs/open-cuts-reporter/1.0.2/index.html` in [your favorite browser](https://www.mozilla.org/en-US/firefox/).

## Development

```bash
$ npm install # to install dependencies
$ npm update # to update dependencies
$ npm audit fix # to install security patches
$ npm run lint # to automatically fix coding style issues
$ npm run test # to run unit-tests with coverage reports
$ npm run docs # to build detailed jsdoc documentation
$ npm run build # to compile backwards-compatible code using rollup
```

## History

The library was originally developed for use in the [UBports Installer](https://github.com/ubports/ubports-installer), but it might be useful to other projects. Semantic versioning will ensure API stability for public functions.

## License

Original development by [J. Sprinz](https://spri.nz). Copyright (C) 2020-2021 [UBports Foundation](https://ubports.com).

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
