"use strict";

/*
 * Copyright (C) 2020-2021 UBports Foundation <info@ubports.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { GraphQLClient, gql } from "graphql-request";

/**
 * @typedef OpenCutsRun
 * @property {String} date date string
 * @property {String} result PASS WONKY FAIL
 * @property {String} comment run comment
 * @property {Array<OpenCutsLog>} logs run logs
 * @property {Array<OpenCutsCombination>} combination run combinations
 */

/**
 * @typedef OpenCutsLog
 * @property {String} name log name
 * @property {String} content log content
 */

/**
 * @typedef OpenCutsCombination
 * @property {String} variable combination variable
 * @property {String} value combination value
 */

/**
 * report runs to the open crowdsourced user testing suite
 * @extends GraphQLClient
 */
export class OpenCutsReporter extends GraphQLClient {
  constructor({ url, token }) {
    super(`${url}/graphql`, { headers: token ? { authorization: token } : {} });
    this.openCutsUrl = url;
  }

  /**
   * login via token
   * @param {String} token user token
   * @returns {Promise}
   */
  setToken(token) {
    this.setHeader("authorization", token);
    return this.request(
      gql`
        {
          me {
            id
          }
        }
      `,
      {}
    )
      .then(({ me }) => {
        if (me?.id) return;
        else throw new Error("invalid token");
      })
      .catch(error => {
        throw new Error(`Failed to set token: ${error}`);
      });
  }

  /**
   * login via credentials
   * @param {String} email login email
   * @param {String} password login password
   * @returns {Promise}
   */
  login(email, password) {
    return this.request(
      gql`
        query login($email: String!, $password: String!) {
          login(email: $email, password: $password)
        }
      `,
      { email, password }
    )
      .then(({ login }) => this.setToken(login))
      .catch(error => {
        throw new Error(`Failed to log in: ${error}`);
      });
  }

  /**
   * create a run for a given systemId and testId. buildId is detected automatically from a build tag.
   * @param {String} systemId system under test
   * @param {String} testId test that was run
   * @param {String} tag build tag
   * @param {OpenCutsRun} run run object
   * @returns {Promise<String>} run url
   */
  smartRun(systemId, testId, tag, run) {
    return this.request(
      gql`
        mutation smartRun(
          $testId: ID!
          $systemId: ID!
          $tag: String!
          $run: RunInput!
        ) {
          smartRun(testId: $testId, systemId: $systemId, tag: $tag, run: $run) {
            id
          }
        }
      `,
      { testId, systemId, tag, run }
    )
      .then(({ smartRun }) => `${this.openCutsUrl}/run/${smartRun.id}`)
      .catch(error => {
        throw new Error(`Failed to create run: ${error}`);
      });
  }
}
