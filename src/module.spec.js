// const { OpenCutsReporter } = require("../lib/module.cjs");
import { jest } from "@jest/globals";
import { OpenCutsReporter } from "./module.js";

describe("class OpenCutsReporter", () => {
  describe("constructor()", () => {
    it("should construct anonymous", () => {
      const openCutsReporter = new OpenCutsReporter({
        url: "https://ubports.open-cuts.org"
      });
      expect(openCutsReporter).toHaveProperty(
        "openCutsUrl",
        "https://ubports.open-cuts.org"
      );
      expect(openCutsReporter).toHaveProperty(
        "url",
        "https://ubports.open-cuts.org/graphql"
      );
    });

    it("should construct authorized", () => {
      const openCutsReporter = new OpenCutsReporter({
        url: "https://ubports.open-cuts.org",
        token: "asdf"
      });
      expect(openCutsReporter).toHaveProperty(
        "openCutsUrl",
        "https://ubports.open-cuts.org"
      );
      expect(openCutsReporter).toHaveProperty(
        "url",
        "https://ubports.open-cuts.org/graphql"
      );
    });
  });

  describe("setToken()", () => {
    it("should set token and resolve", () => {
      const openCutsReporter = new OpenCutsReporter({
        url: "https://ubports.open-cuts.org"
      });
      openCutsReporter.request = jest
        .fn()
        .mockResolvedValue({ me: { id: "1234" } });
      expect(openCutsReporter.setToken("1337")).resolves.toEqual(undefined);
    });
    it("should reject on invalid token", () => {
      const openCutsReporter = new OpenCutsReporter({
        url: "https://ubports.open-cuts.org"
      });
      openCutsReporter.request = jest.fn().mockResolvedValue({});
      expect(openCutsReporter.setToken("1337")).rejects.toThrow(
        "Failed to set token: Error: invalid token"
      );
    });
    it("should reject on error", () => {
      const openCutsReporter = new OpenCutsReporter({
        url: "https://ubports.open-cuts.org"
      });
      openCutsReporter.request = jest.fn().mockRejectedValue("oh no");
      expect(openCutsReporter.setToken("1337")).rejects.toThrow(
        "Failed to set token: oh no"
      );
    });
  });

  describe("login()", () => {
    it("should set token and resolve", () => {
      const openCutsReporter = new OpenCutsReporter({
        url: "https://ubports.open-cuts.org"
      });
      openCutsReporter.request = jest
        .fn()
        .mockResolvedValue({ me: { id: "1234" } });
      expect(openCutsReporter.login("a@b.com", "secure")).resolves.toEqual(
        undefined
      );
    });
    it("should set token and resolve", () => {
      const openCutsReporter = new OpenCutsReporter({
        url: "https://ubports.open-cuts.org"
      });
      openCutsReporter.request = jest.fn().mockRejectedValue("some error");
      expect(openCutsReporter.login("a@b.com", "secure")).rejects.toThrow(
        "Failed to log in: some error"
      );
    });
  });

  describe("smartRun()", () => {
    it("should resolve run url", () => {
      const openCutsReporter = new OpenCutsReporter({
        url: "https://ubports.open-cuts.org"
      });
      openCutsReporter.request = jest.fn().mockResolvedValue({
        smartRun: {
          id: "a"
        }
      });
      return openCutsReporter
        .smartRun(
          "5e9d746c6346e112514cfec7",
          "5e9d75406346e112514cfeca",
          "0.7.4-beta"
        )
        .then(r => {
          expect(r).toEqual("https://ubports.open-cuts.org/run/a");
        });
    });
    it("should reject error", () => {
      const openCutsReporter = new OpenCutsReporter({
        url: "https://ubports.open-cuts.org"
      });
      openCutsReporter.request = jest
        .fn()
        .mockRejectedValue(new Error("something bad"));
      expect(openCutsReporter.smartRun()).rejects.toThrow("a");
    });
  });
});
